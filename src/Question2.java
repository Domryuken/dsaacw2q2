
public class Question2
{
    
    public static void question2()
    {
        /*
        Running the test for each of the G functions
        */
        Gtest();
        G2test();
        G3test();
    }   
    
    public static void Gtest()
    {
        double temp;
        System.out.println("function G(n)");
        for(int n=10;n<=15;n++)
        {
            long t1=System.nanoTime();
            temp = functionG(n);
            long t2=System.nanoTime()-t1;
            System.out.println("G("+n+") = " + temp + "\t Time Taken: " + t2);
        }        
    }

    public static void G2test()
    {
        double temp;
        System.out.println("function G2(n)");
        for(int n=10;n<=1000;n*=10)
        {
            long t1=System.nanoTime();
            temp = functionG2(n);
            long t2=System.nanoTime()-t1;
            System.out.println("G2("+n+") = " + temp + "\t Time Taken: " + t2);
        }
        int i = 4000;
        long time=System.nanoTime();
        temp = functionG2(i);
        long time2=System.nanoTime()-time;
        System.out.println("G2(4000) = " + temp + "\t Time Taken: " + time2);    
    }
    
    
    
    
    
    
    
    public static void G3test()
    {
        double temp;
        System.out.println("function G3(n)");
        for(int n=10;n<=1000000;n*=10)
        {
            long t1=System.nanoTime();
            temp = functionG3(n);
            long t2=System.nanoTime()-t1;
            System.out.println("G3("+n+") = " + temp + "\t Time Taken: " + t2);
        }   
    }
    
    public static double functionG(int n)
    {
        return functionF(n,n);
    }
    public static double functionF(int i, int j)
    {
        if(i==0&&j==0)
        {
            return 0;
        }
        else if(i==0||j==0)
        {
            return 1;
        }
        else
        {
            return (functionF(i-1,j) + functionF(i-1,j-1) + functionF(i,j-1)) / 3;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    THIS IS MY FIRST ATTEMPT AT MAKING AN EFFICIENT ALGORITHM, BUT IT WILL ONLY
    WORK UP TO 4000, BECAUSE IT USES A 2D ARRAY AND USES I AND J AS 
    COORDINATES. THE PROBLEM WITH THIS IS AN ARRAY OF 1000000 BY 1000000
    IS A BIT EXCESSIVE.
    This will only work up to around 4000
    */
    public static double [][] results = null;    
    public static double functionG2(int n)
    {
        results = new double [n][n];
        return functionF2(n,n);
    }
    public static double functionF2(int i, int j)
    {
        if(i==0&&j==0)
        {
            return 0;
        }
        else if(i==0||j==0)
        {
            return 1;
        }
        /*
        if results[i-1][j-1] is 0 then it has not yet had a number saved to it
        so it will get the result and save it there and then return that value
        */
        else if(results[i-1][j-1]==0)
        {
            results[i-1][j-1] = (functionF2(i-1,j) 
                    + functionF2(i-1,j-1) + functionF2(i,j-1)) / 3;
            return results[i-1][j-1];
        }
        /*
        if the results[i-1][j-1] is not 0 then a number has been saved there, 
        so instead of calculating the result, just grab it from the array.
        */
        else
        {
            return results[i-1][j-1];
        }
    }
    
    
    
    
    
    
    
    
    
    /*
    Final version of the algorithm
    
    Tree of values when i=3 and j=3
                         _____
                         |3,3|
                  _____  _____  _____ 
                  |3,2|  |2,2|  |2,3|
           _____  _____  _____  _____  _____
           |3,1|  |2,1|  |1,1|  |1,2|  |1,3|
    _____  _____  _____  _____  _____  _____  _____
    |3,0|  |2,0|  |1,0|  |0,0|  |0,1|  |0,2|  |0,3|
    
    */
    public static double functionG3(int n)
    {
        /*
        creates an array that is to the size of number given plus 1
        and sets the first value to 0, because the bottom center value will
        always be 0, and then fills the rest with 1's.
        (i-1,j) and (i,j-1) are reversed when i==j only one half the tree
        of values is needed.
        As the algorithm goes on more and more of the array spaces is not used
        */
        double [] current = new double[n+1];
        current[0]=0;
        for(int i=1;i<=n;i++)
        {
            current[i]=1;
        }
        
        //this is the number of values in the next layer of the tree
        int end = n; 
        
        /*
        goes through a loop once for every layer of the tree of values
        every time it goes through these loops the portion of the array that is
        relevent will reduce by 1, until eventually there is only one value left
        in position [0] which is the value that is returned.
        */
        for(int j=0;j<n;j++)
        {
            /*
            from the first value in the array to the current value of end, 
            which is the amount of number in the above layer.
            */
            for(int i=0;i<end;i++)
            {
                
                /*
                if i is 0 get the mean of the first value of teh array and two
                the value next to it because the results are mirrored
                */
                if(i==0)
                {
                    current[0]=(current[i]+current[i+1]+current[i+1])/3;
                }
                
                /*
                if not then get the mean of i-1, i and i+1 of current array,
                i is then updated on each iteration so that on the next
                iteration 
                */
                else
                {
                    current[i]=(current[i-1]+current[i]+current[i+1])/3;
                }
            }
            
            /*reduce the value of end by one before entering the next loop.
            */
            end--;
        }
        
        /*
        returns final value which is in the first position fo the array
        */
        return current[0];
    }
}
